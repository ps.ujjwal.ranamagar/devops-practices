Use the following command to setup alias for naming convention

`alias ghash='git log -1 --format=%cd-%h --abbrev=8 --date=format:%y%m%d'`

After setting up alias, use the following command to build the image to use the in-memory database.

`docker build -t assignment:h2 -t --build-arg GITHASH=$(ghash) --build-arg PROFILE=h2 .`

To run this container, use the following command.

`docker run -d --name assignmenth2 assignment:h2`

Then run the following command to build images that connect with mysql.

`docker build -t assignment:$(ghash) -t assignment:latest --build-arg GITHASH=$(ghash) .`

When the images are built, use the following command to set up two containers for application and mysql database.

`docker-compose -f compose.yaml up -d`


