#!/bin/bash

# This script works by counting the number of arguments

set -e

commithash=$(git log -1 --abbrev=8 --format=%cd-%h --date=format:%y%m%d)

if [[ ! -e target/assignment-${commithash}.jar ]]
then
  mvn clean package
fi

if [[ $# -eq 0 ]]
then
  for i in $(docker ps --format '{{.Names}}')
  do
    if [[ ${i} = "assignmenth2" ]]
    then
      echo -e "\033[1mStopping container ${i} to free up port 8090\033[0m"
      docker stop $i > /dev/null
    fi
  done
  echo -e "\033[1mEnvironment variable SPRING_PROFILES_ACTIVE=h2 will not be set.\033[0m"
  docker build -t assignment:${commithash} -t assignment:latest --build-arg GITHASH=${commithash} .
  docker-compose -f docker-compose.yaml up -d
  exit
elif [[ $# -eq 1 ]]
then
  for i in $(docker ps --format '{{.Names}}')
  do
    if [[ ${i} = "app" ]]
    then
      echo -e "\033[1mStopping container ${i} to free up port 8090\033[0m"
      docker stop $i > /dev/null
    fi
  done
  echo -e "\033[1mEnvironment variable SPRING_PROFILES_ACTIVE=h2 will be set.\033[0m"
  docker build -t assignment:${commithash} -t assignment:h2 --build-arg GITHASH=${commithash} --build-arg PROFILE=h2 .
  docker-compose -f docker-compose-h2.yaml up -d
else
  echo "Skipped building because more than one arguments were passed"
fi