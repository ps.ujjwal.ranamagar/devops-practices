Insert new data

`curl -X POST 127.0.0.1:8090/persons/ -d '{"name":"John","age":"11"}' -H "Content-Type:application/json"`

`curl -X POST 127.0.0.1:8090/persons/ -d '{"name":"David","age":"12"}' -H "Content-Type:application/json"`

`curl -X POST 127.0.0.1:8090/persons/ -d '{"name":"Leo","age":"13"}' -H "Content-Type:application/json"`

Update record of a specific person

`curl -X PUT 127.0.0.1:8090/persons/1 -d '{"name":"John","age":"14"}' -H "Content-Type:application/json"`

Show records

`curl -X GET 127.0.0.1:8090/persons/`

Show record of a specific person

`curl -X GET 127.0.0.1:8090/persons/1`

Delete records of a specific person

`curl -X DELETE 127.0.0.1:8090/persons/2`