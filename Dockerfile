# Use a small, official, updated and secure base image.
FROM openjdk:19-jdk-alpine3.16

# Set up build arguments for naming convention
ARG PROFILE=mysql
ARG GITHASH

# Set environment variables for the application profile and JAVA options.
ENV SPRING_PROFILES_ACTIVE=$PROFILE
ENV HASH=$GITHASH

# Create a directory to save the jar in
RUN mkdir -p /home/app
WORKDIR /home/app

# Set a non-root user inside the container.
RUN adduser -D myuser

# Grant user assignment ownership of the newly created directory
RUN chown -R myuser:myuser /home/app/

# Copy necessary files 
COPY ./target/assignment-$GITHASH.jar /home/app/assignment-$GITHASH.jar

# Port expose
EXPOSE 8090

# Start the application
USER myuser
CMD java -jar assignment-${HASH}.jar







