1. Use your own words; what does JVM mean?    (1 Point)

    `JVM stands for Java Virtual Machine. It is a crucial component of the Java programming language that enables Java code to run on various hardware and software platforms. The JVM acts as a virtual computer that interprets compiled Java code and executes it. It provides an environment that abstracts the low-level hardware details, allowing Java programs to be platform-independent.`


2. What does “application server” mean?    (1 Point)

    `An application server is a software platform that provides the infrastructure and services necessary to deploy, manage, and run web applications. It acts as an intermediary between the backend systems and the front-end web browser or mobile application, processing and managing requests, and delivering responses to the end-users.`


3. Use your own words; what is a WAR file, and how tomcat deals with it? And where it should be to be deployed?    (1 Point)

    `A WAR (Web Application Archive) file is a type of file format used for packaging and deploying web applications in Java. It contains all the necessary files and resources, including HTML pages, Java classes, configuration files, and other static assets required for the web application to run.`

    `When a WAR file is deployed on Tomcat, the server first unpacks the contents of the file and creates a directory structure with the same name as the WAR file. This directory contains all the necessary files and resources that are required for the web application to run. Tomcat then loads the web application and deploys it on its server.`

    `To deploy a WAR file on Tomcat, it should be placed in the "webapps" directory, which is located in the Tomcat installation directory. Once the WAR file is copied to this directory, Tomcat automatically detects it and deploys the web application. After the web application is deployed, it can be accessed using the URL that corresponds to the application's context root.`


4. Let’s assume that you have a Java web application, and this application needs to be deployed on a server that is totally fresh and does not have any pre-prepared infrastructure. List the steps needed to deploy this web application (over tomcat application server)?    (1 Point)

    `To deploy deploy a Java applicatio in a fresh server, Java must be installed first. Following steps can be taken to install Java in a Ubuntu:`
        
    - Run `sudo apt-get update` to check for updates
    - Run `sudo apt install default-jdk` to install java
    - Run `java -version` to check java version
    - Run `update-alternatives --config java` to locate where apt installed Java on Ubuntu
    - Run `sudo vim /etc/environment` then copy paste `JAVA_HOME="/lib/jvm/java-11-openjdk-amd64/bin/java"` and save it
    - Run `source /etc/environment` to force the Ubuntu terminal to reload the environment configuration file
    - Run `echo $JAVA_HOME` to get the path of java

    `After installation of Java is completed, tomcat can be installed in following steps:`

    - Click on the <a href="https://dlcdn.apache.org/tomcat/tomcat-8/v8.5.87/bin/apache-tomcat-8.5.87.tar.gz">link</a> to download tomcat-8.5.87 tar.gz file </br>
    - Make a directory named `tomcat` and copy the extracted file to that directory.
    - Go to `tomcat/apache-tomcat-8.5.87/bin/` and run `startup.sh`. The welcome page should be available at <a href="http://127.0.0.1:8080/">127.0.0.1:8080</a>
    - Go to `tomcat/apache-tomcat-8.5.87/conf/` and run `sudo vim tomcat-users.xml`. Then paste the below html syntax and save it.\
        `<role rolename="admin-gui"/>`\
        `<role rolename="manager-gui">`\
        `<user username="tomcat" password="pass" roles="admin-gui,manager-gui"/>`
    - Then you should be able to login using the username and password.


5. Use your own words; is running multiple instances of tomcat on one server, benefits or not? And why?   (1 Point)

    There is both benefits and drawbacks of running multple instances of tomcat on one server which are discussed below:

    `Benefits:`

    - Running each Tomcat instance in a separate process can improve application isolation. This implies that the other instances won't be impacted if one instance has a problem or crashes.`

    - As each instance can be configured with its own resource constraints, running many instances allows we to more effectively allocate resources (such CPU and memory).

    - Applications may be scaled more simply while running in several instances since we can add or delete instances as needed.

    `Drawbacks:`

    - As each instance needs its own resources (such as CPU and memory), running several instances may result in a higher demand for system resources.

    - Compared to maintaining a single instance, managing numerous instances might be more difficult and time-consuming.

    - Conflicts over ports might arise if we run many Tomcat instances on the same server because they all by default utilize the same ports (such port 8080). To prevent conflicts, we must set up each instance to utilize a distinct port.


6. You have two Java web applications, and one of them shall be accessible from outside the server, and one shall not be accessible except from localhost. How to configure this? And what is the file to configure? (Write a sample down).    (2 Points)

    For the web application that needs to be accessible from outside the server, we can specify the address attribute of the Connector element to listen on a specific IP address or on all available IP addresses in `server.xml` file. To listen on all available IP addresses, we can set the address attribute to an empty string or to 0.0.0.0. For example:

    `<Connector port="8080" protocol="HTTP/1.1"
           connectionTimeout="20000"
           redirectPort="8443" 
           address="" />`
    
    And to make accessible from only localhost, we can set the address attribute to an empty string or to 127.0.0.1 For example:
    
    `<Connector port="8080" protocol="HTTP/1.1"
           connectionTimeout="20000"
           redirectPort="8443"
           address="127.0.0.1" />`


7. Practical: initiate two instances of tomcat 8, and run one of them over HTTPS, and the other over HTTP. (You can use the default application installed with tomcat), (Use self-signed certificate). Note: question answers will be reviewed after the assignment is delivered!    (10 Points)

    To run multiple instances of tomcat on the same server following steps can be taken:

    - Make two users. Let's say "tomcat1" and "tomcat2". After the users are created, install tomcat in each user locally. Then we can create systemd service files to make startup of both tomcat instances.
        
        `cd /etc/systemd/system`  
        `touch tomcat1.service tomcat2.service`
            
    - Then setup the service files. 

        `[Unit]`  
        `Description=Run first instance of tomcat`  
        `After=network.target`  
        `[Service]`  
        `Type=forking`  
        `Environment="JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/"`  
        `Environment="CATALINA_HOME=/home/tomcat1/apache-tomcat-8.5.87/"`   
        `User=tomcat1`   
        `Group=tomcat1`  
        `ExecStart=/bin/bash /home/tomcat1/apache-tomcat-8.5.87/bin/startup.sh`  
        `ExecStop=/bin/bash /home/tomcat1/apache-tomcat-8.5.87/bin/shutdown.sh`  
    
    - Generate RSA signing certificate

        `keytool -genkey -keyalg RSA -alias tomcat2 -keystore keystore.jks` 

    - Configure server.xml file to make tomcat2 use the generated certificate:

        `<Connector port="8082" protocol="HTTP/1.1"`  
            `SSLEnabled="true" maxThreads="150"`  
            `scheme="https" secure="true" clientAuth="false" sslProtocol="TLS"`  
            `keystoreFile="/home/tomcat2/keystore.jks"`  
            `keystorePass="clusus"`  
            `keyAlias="tomcat2"/>`  


